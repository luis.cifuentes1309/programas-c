#include <stdlib.h>
#include <stdio.h>

int suma (int op1, int op2)//firma o prototipo de la función
{
    return op1 + op2;
}
int resta (int op1, int op2)
{

    return op1 - op2;
}

/*********************/
/*   CALCULOS        */
/*********************/
int main(){
/*DECLARACION DE VARIABLES*/

    int op1,
        op2,
        result;


/*ENTRADA DE DATOS*/
printf("Operando 1: ");
scanf(" %i", &op1);

printf("Operando 2: ");
scanf(" %i", &op2);

/*CALCULOS*/
result = suma(op1, op2);


/*SALIDA DE DATOS*/
printf("%i + %i = %i\n", op1, op2, result);

return 0;
}
