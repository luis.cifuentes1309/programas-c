#include <stdio.h>

void imprime (int n) {
	if (n<1)
		return;

	printf("%i  ",n);

	imprime (n-1);
}

int main (){

	imprime (5);

	printf("\n");

	return 0;
}
