#include <iostream>
#include <stdlib.h>

using namespace std;

	class Rectangulo{
		
		public:

		unsigned Lado,Ancho; //Al declarar variables las ponemos con mayusculas para diferenciarlas de los constructores

		void perimetro(); //Método permitero

		void area(); //Método área

		Rectangulo (unsigned lado,unsigned ancho){ //Creamos constructor 

			Lado = lado;
			Ancho = ancho;

		}
		
		void medidas_rectangulo(){

			cout << "El lado del rectangulo es " << Lado << "\n";
		}
	};

int main(){

	Rectangulo /*objeto cuadrado1*/ rectangulo1 = Rectangulo (40,40); //Pasamos parametros al constructor 
	
	rectangulo1.medidas_rectangulo();
	

	return EXIT_SUCCESS;
}
