#ifndef Ordenador_h
#define Ordenador_h

#include <iostream>
#include <stdio_ext.h>
using namespace std;

class Ordenador{
    string Marca;
    string Procesador;
    int Peso;
    string Sistema_operativo;
    bool Encender;

public:
//Incializamos constructor
    Ordenador();
   
    
//Creamos el constructor parametrizado para pasar los atributos que tiene la clase al objeto
    Ordenador(string marca,string procesador,int peso,string sistema_operativo);
    

//Usamos los metodos get para acceder a los atributos y set para definir sus valores


   void setMarca(string marca);
    
   string getMarca();


   void setProcesador(string procesador);
   
   string getProcesador();
   
   
   void setPeso(int peso);
   
   int getPeso();
   
   
   void setSistema_operativo(string sistema_operativo);
   
   string getSistema_operativo();
   
   
   void caracteristicas();
   
   void ordenador_encendido();
   
};

#endif
