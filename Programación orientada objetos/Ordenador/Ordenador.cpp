#include <iostream>
#include <stdio_ext.h>
using namespace std;
#include "Ordenador.h"


void Ordenador::setMarca(string marca){
        Marca = marca; 
    }
    
    string Ordenador::getMarca(){
        return Marca;
    }


   void Ordenador::setProcesador(string procesador){
       Procesador = procesador;
   }
   
   string Ordenador::getProcesador(){
       return Procesador;
   }
   
   
   void Ordenador::setPeso(int peso){
       Peso = peso; 
   }
   
   int Ordenador::getPeso(){
       return Peso;
   }
   
   void Ordenador::setSistema_operativo(string sistema_operativo){
       Sistema_operativo = sistema_operativo;
   }
   
   string Ordenador::getSistema_operativo(){
       return Sistema_operativo;
   }
   

Ordenador::Ordenador()
{
        string Marca = ""; 
        string Procesador = "";
        int Peso = 0;
        string Sistema_operativo = "";
        bool Encender = "";
} 

Ordenador::Ordenador(string marca,string procesador,int peso,string sistema_operativo)
{
        Marca = marca;
        Procesador = procesador;
        Peso = peso;
        Sistema_operativo = sistema_operativo;
}

void Ordenador::caracteristicas(){
    cout << "La marca del ordenador1 es " << getMarca() 
    << ", el procesador " << getProcesador() 
    << ", pesa " << getPeso() << "kg" 
    << " y su sistema operativo es "<< getSistema_operativo() << ".\n";
}

void Ordenador::ordenador_encendido()
{
    char si_o_no;
    Encender = false;
    
    
        printf("\nEl ordenador está apagado \n¿Quieres encenderlo?(s/n)\n");
        scanf("%c",&si_o_no);
        
        if (si_o_no == 's' || si_o_no == 'S'){
        Encender = true;
        printf("Has encendido el ordenador\n");
       
        }if (si_o_no == 'n' || si_o_no == 'N'){
        printf("No has encendido el ordenador\n");
            
        }
        __fpurge(stdin);
}
