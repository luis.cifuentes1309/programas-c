#include <iostream>

using namespace std;

static const double PI = 3.1416;


class Figura
{

    protected:
        double area;   
  
    public:
        Figura(){}   
        
        static void dameAreaVector(Figura* v[], int numElementos)
        {
   
            for (int i = 0; i  < numElementos; i++)
            {
                v[i]->dameArea();
            }
        }
    
    virtual void dameArea() = 0; 
};

class Cuadrado: public Figura
{

    private:
        int lado;
      
    public:
    
    void setLado(int Lado){
        lado = Lado;
    }
    
    int getLado(){
        return lado;
    }
    
    
    void dameArea()
    {
        cout << "¿Cuanto mide el lado del cuadrado? \n";
        cin >> lado;
        
        
        area = lado * lado;
        cout << "El area del cuadro es " << area << "\n";
    }
};

class Circulo: public Figura{
    
  private:
    double radio;
  
  public:
    void dameArea()
    {
        cout << "¿Cuanto mide el radio del círculo? \n";
        cin >> radio;
        
        area = PI * (radio * radio );
        cout << "El area del círculo es " << area << "\n";
        
    }
};

int main()
{

   Figura *v[2];
   
   Cuadrado cuadrado1;
   Circulo circulo1;
   
   v[0] = &cuadrado1;
   v[1] = &circulo1;
   
   Figura::dameAreaVector(v, 2);

    return 0;
}
