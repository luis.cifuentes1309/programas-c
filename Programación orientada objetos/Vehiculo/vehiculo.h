#ifndef vehiculo_h
#define vehiculo_h

#include <iostream>
using namespace std;

class Vehiculo{
  protected:
    int numeroRuedas;
    string funcionamiento;
  
  public:
    Vehiculo (int ,string);
    
    virtual void caracteristicas() = 0;
    
    void setNumeroRuedas(int);
    
    int getNumeroRuedas();
    
};


class Bicicleta:public Vehiculo{
    private:
      string timbre;
      int numRuedines;
    
    public: 
      Bicicleta(int ,string ,string ,int );
      
      void caracteristicas();
      
      void ruedines();
};

#endif