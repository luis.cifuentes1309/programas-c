#include <iostream>
#include "vehiculo.h"
using namespace std;

    Vehiculo::Vehiculo (int _numeroRuedas,string _funcionamiento):numeroRuedas(_numeroRuedas),funcionamiento(_funcionamiento){}
    
    
    void Vehiculo:: setNumeroRuedas(int _numeroRuedas){
        numeroRuedas = _numeroRuedas;
    }
    
    int Vehiculo:: getNumeroRuedas(){
        return numeroRuedas;
    }
    
    
    Bicicleta::Bicicleta(int _numeroRuedas,string _funcionamiento,string timbre,int numRuedines):
    Vehiculo(_numeroRuedas,_funcionamiento),timbre(timbre),numRuedines(numRuedines){}
      
    
    void Bicicleta:: caracteristicas(){
        cout << "La bicicleta tiene " << numeroRuedas << " ruedas ,funciona por " << funcionamiento << " y tiene " << timbre <<".\n";
        cout << "Usuando constructor: Las bicis con ruedines tienen " << numRuedines << " ruedines\n";
    }
    
    void Bicicleta::ruedines(){
        cout << "Usando getters y setters: Las bicis con ruedines tienen " << getNumeroRuedas() << " ruedines\n"; 
    }
