#include <iostream>
#include "vehiculo.h"

using namespace std;

int main()
{
    Bicicleta bicicleta(2,"pedales","timbre",4);
    bicicleta.caracteristicas();
    bicicleta.setNumeroRuedas(4);
    bicicleta.ruedines();
    
    return 0;
}