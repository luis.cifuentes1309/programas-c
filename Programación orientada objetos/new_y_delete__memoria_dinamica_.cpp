
#include <iostream>

using namespace std;

int main()
{
    int tamanio;
    
    cout << "Introduce el tamaño del array\n";
    cin >> tamanio;
    
    int *array = new int[tamanio*sizeof(int)];
    
    cout << "\nIntroduce los valores del array\n";
    
        
    for (int i = 0;i<tamanio;i++){
         cout << "array[" << i << "]: ";
         cin >> array[i];
       
        
    }
    
    cout << "\n";
    
    for (int i = 0;i<tamanio;i++)
    cout << *(array + i) << " "; // *(array + i) = array[i] (son notaciones diferentes pero hacen lo mismo)
        
    delete[] array;
    array = NULL;


    return 0;
}
