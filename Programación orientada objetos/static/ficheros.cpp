/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <iostream>

using namespace std;

class Fichero{
  private:
    static int ficherosCreados;
    string nombre;
    double tamano;

    
  public:
  
     Fichero(){
        ficherosCreados++;//incrementamos para que sume todos los objetos que tenemos

    }
    
    static int getFicherosCreados(){
        return ficherosCreados;
    }
    
    void setNombre(string Nombre){
        nombre = Nombre;
    }
    
    string getNombre(){
        return nombre;
    }
    
    void setTamano(double Tamano){
        tamano = Tamano;
    }
    
    double getTamano(){
        return tamano;
    }
    
    ~Fichero(); //Llamada del destructor
    
   
};

int Fichero::ficherosCreados;

Fichero::~Fichero(){ //Destructor
    ficherosCreados--; //decrementamos para que reste los objetos destruidos
}

int main()
{
    Fichero f1,f2,f3,f4,f5;
    
    f1.setNombre("Prueba.txt");
    f1.setTamano(3.56);
    cout << "El nombre del primer fichero es " << f1.getNombre() << ", el tamaño es " << f1.getTamano() << "GB y el numero de ficheros totales es " << f1.getFicherosCreados();
    printf("\n\n");
    f1.~Fichero();
    f2.~Fichero();
    f3.~Fichero();
    f4.~Fichero();
    
    cout << "El numero de ficheros no destruidos es " << Fichero::getFicherosCreados();
    
    return 0;
}



