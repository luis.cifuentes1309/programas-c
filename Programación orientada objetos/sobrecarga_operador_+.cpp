#include <iostream>
using namespace std;

class Suma {
public:
    double atributo1, atributo2;

    // constructor parametrizado
    Suma(const double Atributo1,const double Atributo2)
    {
        atributo1 = Atributo1;
        atributo2 = Atributo2;
    }
};

// Sobrecarga del operador +
Suma operator +( Suma const &p1, Suma const &p2){ 
    //ponemos const para no poder cambiarlos dentro del codigo
    // pasamos Suma por referencia (el &), para evitar que se hagan copias innecesarias.
                                            
    Suma resultado( p1.atributo1 + p2.atributo1, p1.atributo2 + p2.atributo2 );
    return resultado;
}

int main()
{
    Suma objeto1(50, 100);
    Suma objeto2(150, 175);
    Suma Z = objeto1 + objeto2;

    cout << "atributo1 = " << objeto1.atributo1 << ',' << objeto1.atributo2 << "\n";
    cout << "atributo2 = " << objeto2.atributo1 << ',' << objeto2.atributo2 << "\n";
    cout << "resultado = " << Z.atributo1 << ',' << Z.atributo2 << "\n";

    return 0;
}
