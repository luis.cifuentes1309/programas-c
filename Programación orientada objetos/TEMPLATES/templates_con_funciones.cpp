#include <iostream>
using namespace std;

template <typename T>
void Intercambiar(T& x, T& y){
    T z = x;
    x = y;
    y = z;
}

int main(){
    int num1 = 45, num2 = 35;
    cout << "num1 = " << num1 << ", num2 = " << num2 << "\n";

    cout << "\nIntercambiando valores ...\n\n";
    Intercambiar(num1, num2);

    cout << "num1 = " << num1 << ", num2 = " << num2 << "\n";
    
    char a = 'a', b = 'b';
    cout << "\na = " << a << ", b = " << b << "\n";
    
    cout << "\nIntercambiando valores ...\n\n";
    Intercambiar(a, b);
   
    cout << "a = " << a << ", b = " << b << "\n";

    return EXIT_SUCCESS;
}


