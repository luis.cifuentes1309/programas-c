#include <stdio.h>


int main(){

int num;

printf("Introduzca un numero\n");
scanf("%i",&num);

if (num > 0){
	printf ("El numero es positivo \n");
}

else if (num == 0){	//Estructura if dentro de otra estructura if 
			//siempre se pone antes del else a secas

        printf ("El número es neutro \n");
}

else{			//Lo contrario del if principal 

	printf ("El numero es negativo \n");
}


return 0;
}
