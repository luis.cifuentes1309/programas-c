#include <stdio.h>
#include <stdlib.h>
#define MAX 10
int main ()
{

//EJ 1
int tamaño_array[MAX];	//DECLARACION ARRAY
	
	printf("El tamaño del array es: %li bytes\n", sizeof(tamaño_array)); //TAMAÑO QUE OCUPA EL ARRAY

//EJ 2
int array[MAX];//DECLARACIÓN ARRAY
 
 	/*DECLARACIÓN VARIABLES*/   
    int tamaño_elemento, tipo_dato, elementos;

	/*ASIGNACIONES*/
    tamaño_elemento = sizeof(array); //CUANTO OCUPA EL ARRAY
    tipo_dato = sizeof(int); // CUANTO OCUPA EL INT
    
    /*OPERACIONES*/
    elementos = tamaño_elemento / tipo_dato; 
    
    /*SALIDA DE DATOS*/
    printf("La cantidad de elementos de un array es de: %i \n", elementos);

//EJ 3

/*APARTIR DEL TERCER LONG SALTA error: ‘long long long’ is too long for GCC*/
int a;
long int b;
long long int c;

/*APARTIR DEL SEGUNDO LONG SALTA error: ‘long long’ specified with ‘double’*/
double d;
double long e;

printf ("El tamaño de int a es: %lu \n", sizeof (a));
printf ("El tamaño de int long b es: %lu \n", sizeof (b));
printf ("El tamaño de int long long c es: %lu \n", sizeof (c));
printf ("El tamaño de double d es: %lu \n", sizeof (d));
printf ("El tamaño de double long e es: %lu \n", sizeof (e));

return 0;}
