#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int main () {
//EJ 1
int numero;     //Definimos variable

/*VALORES DE ENTRADA*/
printf ("Número: ");
    scanf ("%i", &numero);

/*CONDICIONAL*/
if (numero % 2 == 0)
        printf ("El número %i es par.\n", numero);
    else
        printf ("El número %i es impar.\n", numero);

__fpurge(stdin);//PURGAMOS TUBO SALIDA

//EJ 2
  
    char respuesta; //Definimos variable

    /*VALORES ENTRADA*/ 
    printf("\n¿Quiere instalar algo? s/n: ");
    scanf("%c", &respuesta);

    /*CONDICIONAL*/
    if(respuesta == 's')
        printf("Pues lo instalo \n");
    if (respuesta == 'n')
        printf("Pues no te lo instalo \n");

    __fpurge(stdin);

//EJ 3

char answer;  //Definimos variable

/*VALORES ENTRADA*/
    printf("\n¿Quiere instalar algo? s/n: ");
    scanf("%c", &answer);

char answer_mayuscula = tolower(answer);

/*CONDICIONAL*/
    if(answer_mayuscula == 's')
        printf("Pues lo instalo \n");
    if (answer_mayuscula == 'n')
        printf("Pues no te lo instalo \n");

//EJ 4
#define MAX 3
char tos[MAX];
char maulla[MAX];

printf("\n¿Tiene tos? Responda si o no\n");
scanf("%s",tos);
printf("¿Maulla?Responda si o no\n");
scanf("%s",maulla);



if(strcmp(tos,maulla) == 0)
    printf("Usted esta sano\n");

else
    printf("Usted tiene tos ferina\n");

if(strcmp(tos,maulla) == 0)
    printf("Usted es un gato\n");

if(strcmp(tos,maulla) == 0)
    printf("Usted tiene tos felina\n");

/*NO ENTIENDO PORQUE SI PONGO 2 VECES SI O 2 VECES ME SALEN TODOS LOS DIAGNOSTICOS MENOS EL DEL ELSE QUE SALE CUANDO PONGO SI Y NO O VICEVERSA*/

return 0;
}


