#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <cstdlib>
#include <string.h>

#define   MAX   0x20

int
main ()
{

/*DECLARACIÓN VARIABLES*/
  int entero;
  double real;
  char rango;


/*ENTRADA DE DATOS*/

  //EJ 1
  printf ("Ingresa un número entero \n");
  scanf ("%i", &entero);

// EJ 2
  printf ("Ingresa número real \n");
  scanf ("%lf", &real);
  __fpurge (stdin);
// EJ 3 
  printf ("Introduce un número del 32 al 128 \n");
  scanf ("%[32-128]", &rango);
  __fpurge (stdin);

//EJ 4 
  char hexadecimal[MAX]; //definimos string
  
  /*VALORES DE ENTRADA*/
  printf("Introduce un número en hexadecimal: \n");
  scanf(" %[0-9a-f-A-F]",hexadecimal);
  
  /*SALIDA DE DATOS*/
  printf("El número es: %s \n \n", hexadecimal);
  __fpurge (stdin); //Muy importante, hace que si no pones hexadecimal que no aparezca en el siguiente ej 


//EJ 5
  system ("clear");

//EJ 6 

/*DEFINIR COLORES*/
#define AZUL_BRILLANTE     "\033[1;34;36m"
#define VERDE_NORMAL    "\033[1;32;32m"

/*DECLARACION VARIABLES*/
  int num1,   //numero1
      num2,   //numero2
      result; //resultado

/*VALORES DE ENTRADA*/
   printf("Introduce el primer numero: ");
   scanf("%d",&num1);
   printf("Introduce el segundo numero: ");
   scanf("%d",&num2);
 
/*OPERACIONES*/ 
   result = num1 + num2;

/*SALIDA DE DATOS*/   
   printf(AZUL_BRILLANTE " %d+%d=" VERDE_NORMAL "%d\n",num1,num2,result);
 __fpurge(stdin);//purgamos salida aunque no se porque el siguiente ej se imprime en verde en vez de en blanco


//EJ 8
char nombre[MAX]; //Definimos string

/*VALORES ENTRADA*/
 printf ("Nombre: ");
    fgets (nombre, MAX, stdin);

    int ultima = strlen (nombre);
    nombre[ultima-1] = '\0';

/*SALIDA DE DATOS*/
    printf("Encantado ");
    puts(nombre);

//EJ 10
  
  int memoria;//Definimos variable


printf("&%p: %i (%lu bytes)\n", &memoria, memoria, sizeof (memoria) );


  return 0;
}
