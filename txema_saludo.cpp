#include <stdio.h>
#include <stdio_ext.h>
#include "iostream"
#include <stdlib.h>
#include <cstdlib>
#include <string.h>

#define MAX 0X20


int main(int argc, char *argv[]){

/*DECLARACIÓN VARIABLES*/
char nombre[MAX];

/*VALORES DE ENTRADA*/
printf("¡Bienvenido!\n Ingrese su nombre:");
scanf("%s", nombre);

/*VALORES DE SALIDA*/
printf("Encantado de conocerle %s ", nombre);

__fpurge(stdin);

printf("¡Bienvenido!\n Ingrese su nombre:");
fgets (nombre, MAX, stdin);
printf("%s", nombre);

int ultima = strlen (nombre);
nombre[ultima-1]='\0';

printf("Nombre (yo=<nombre>): ");
scanf (" yo=%s", nombre);
printf ("%s\n", nombre);

return 0;
}

