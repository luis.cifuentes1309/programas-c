#include <stdio.h>

int main()
{
    int array[5];
    
    printf("La direccion de memoria de array %p es la misma que array[0] %p \n",array,&array[0]);
    
     int *puntero;
     puntero = array;
     printf("El puntero %p tiene la misma direccion de memoria que array y array[0] %p\n\n",puntero,array);
     

    //pide valores al usuario
    for(int i = 0; i < 5; i++){
        printf("array[%i]: ",i);
        scanf("%i",&array[i]);
     }
     
     printf("\n");
    
    //muestra los valores del usuario 
    for(int j = 0; j < 5; j++){
         printf("%i ",array[j]);
    }
    
      printf("\n\nNotación de punteros: %i",*(array + 1));
      printf("\nNotación de arrays: %i",array[1]);
      
    printf("\n\n");
    
    //SUMA array[0] + array[1] luego independientemente suma array[1] + array[2] y asi hasta array[3] + array[4]
    for(int k = 0; k < 4; k++){
    array[k] = array[k] + array[k+1];
    printf("%i ",array[k]);
    }

    return 0;
}
