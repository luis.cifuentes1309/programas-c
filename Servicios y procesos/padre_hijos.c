//Padre con 3 hijos
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>

int main(){

       	pid_t pid;
 	int x;

 	for(x=1;x<=3;x++)
 	{
  		pid=fork();

  	if(pid == 0)
  	{
   		printf("soy el hijo %d, mi padre es %d\n",getpid(),getppid());
   		exit (0);
		

  	}

  	else{
    	wait(&pid);
  	}

 	}

 return 0;
}
   
