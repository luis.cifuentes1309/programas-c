#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <errno.h>
#include <stdlib.h>

int main (int argc,char *argv[]){
	
	pid_t pid;
	int tiempo_dormir;
	int estado;
	int numProcesos;
	
	numProcesos = atoi(argv[1]);
		
		if(numProcesos > 0){
		//los parametros están correctos

			for(int i = 0; i < numProcesos; i++){
				pid=fork();

				if(pid == -1){
					printf("error en la creación de procesos %i\n",errno);
				}

				else{
					if(pid == 0){
						//Devuelve un número acorde a una semilla, hace el modulo
						//tiempo_dormir=rand()%31;
						//sleep(tiempo_dormir);
						//printf("Soy el proceso hijo %i estoy ejecutando y he dormido %i\n",getpid(),tiempo_dormir);
						//exit(tiempo_dormir);
						exit(0);
					}

					else{
						wait(&estado);
						printf("%i. Soy el padre con pid %i y mi hijo %i ha dormido %i\n", i, getpid(), pid, estado>>8);
					}
				}
			}
		}
		
		else{
			printf("Los parametros son incorrectos\n");
		}

return 0;
}
