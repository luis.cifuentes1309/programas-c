#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>


bool es_par (int numero) {
	if(numero %2 == 0){
			//par
			return true;
	}

	else{
		//impar
		return false;
	}
}	

int main (int argc,char *argv[]){

	if(argc == 1){
		printf("Faltan parametros");
	}
	
	else{
		for (int i = 1; i < argc; i++){
			if(isdigit(*argv[i])){//isdigit: funcion que comprueba si lo que has introducido es un numero o una letra
				
				if(es_par(atoi(argv[i]))){ //atoi: funcion que convierte de numero a string
					printf("El numero es par %s\n",argv[i]);
				}
				else{
				printf("EL numero es impar %s\n", argv[i]);
				}
			}

				else{
					printf("El parametro no es un numero");
				}

		}
	}

return 0;
}
