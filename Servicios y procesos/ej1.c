#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //libreria procesos

int main (int argc,char *argv[]){

	pid_t pid;//pid_t funciona como int 
	
	pid = fork(); //se realiza una copia
	
	if(pid == -1){
		printf("Se ha generado un error en el procedimiento");//Porque si pid devuelve -1 es que hay un error
		return -1;//Para que termine 
	}

	else{
		if(pid == 0){
			//Hijo
			sleep(1);
			printf("Mi pid es %i y el de mi padre %i \n",getpid(),getppid());
			
		}
		
		else{
			//Padre
			printf("Mi pid es %i y el de mi hijo %i\n",getpid(),pid);
		}
	}

	return 0;
}
