#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int esVocal(char c){
    
    return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' ||c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U');
}

int contarVocales(char *cadena){ //porque char *cadena es un puntero?
    
    int numVocales = 0;
    for(int i=0; i < strlen(cadena); i++){
        if( esVocal(cadena[i]) )
            numVocales++;
    }
    
    return numVocales;
}


int main( int argc,char *argv[]){
    
	int nVocales = 0, posicion = 0;

		for(int i = 1; i < argc; i++)
		{
			if( nVocales < contarVocales(argv[i]) )
			{
				nVocales = contarVocales(argv[i]);
				posicion = i;
			}
		}
	
	printf("El parametro con mas vocales es %s\n", argv[posicion]);

	return 0;
}


