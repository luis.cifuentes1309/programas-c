#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int main (int argc , char *argv []) {
    pid_t pid;
    int numHijos = 0;
    char letra ='`';
    
    for (numHijos = 0; numHijos < 20; numHijos++)
    {
        letra++;
       
        pid = fork ();
    

        if ( pid == -1) {
            printf ("Fallo en fork\n");
            return -1;
        } 
        else {
            if (pid == 0) {
                printf ("Proceso hijo: PID %d Nº Hijos=%i  letra=%c\n", getpid (), numHijos, letra);
            } else {
                printf ("Proceso padre: PID %d \n", getpid());
                sleep(30);
            }
        }
    }
    
    return 0;
}
