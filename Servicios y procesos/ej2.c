#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //libreria procesos
#include <wait.h>

int main (int argc,char *argv[]){

	pid_t pid;//pid_t funciona como int 
	int estado;

	pid = fork(); //se realiza una copia
	
	if(pid == -1){
		printf("Se ha generado un error en el procedimiento");
		return -1;
	}

	else{
		if(pid == 0){
			//Hijo
			
			printf("Mi pid es %i y el de mi padre %i \n",getpid(),getppid());
			exit(1);//Pasa un dato al padre
		
		}
		
		else{
			//Padre
			wait(&estado);//El padre espera al hijo
			printf("Mi pid es %i y el de mi hijo %i\n",getpid(),pid);
			
			printf("El estado de mi hijo es %i\n", estado >> 8);
		}
	}

	return 0;
}
