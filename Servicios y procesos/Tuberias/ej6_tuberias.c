#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>


int main(int argc, char *argv[]){

pid_t pid;
int tuberia[2];

int numero;


pipe(tuberia);
pid=fork();

	if(pid==-1){
	
		printf("ERROR\n");
	}else{
		
		if(pid==0){
			
			wait(NULL);
			printf("Soy el hijo con pid %i y mi padre es %i\n",getpid(),getppid());

			close(tuberia[1]);

                        read(tuberia[0],&numero,1);
			char charValue=numero+'0'; //Esta sumando 17 al codigo SCII del 0 que es 48 por lo que mostrara el caracter ascii que este en el 65
			
			printf("El padre me manda el numero %i y lo convierto a la letra %c \n", numero, charValue);
                        close(tuberia[0]);
			

			pid=fork();
			if(pid==0){
				
				read(tuberia[0],&numero,1);
	                        char charValue=numero+'0'; //Esta sumando 17 al codigo SCII del 0 que es 48 por lo que mostrara el caracter ascii que este en el 65

        	                printf("Mi padre(el hijo) me manda la letra %c y lo convierto al numero %i \n", charValue, numero);

				printf("Soy el nieto con pid %i y mi padre es %i\n",getpid(),getppid());

				exit(0);
			}
			else{
				wait(NULL);
			}
			exit(0);
		
		}else{
		
			close(tuberia[0]);
			
			
			numero = 17;
			write(tuberia[1],&numero,1);

			close(tuberia[1]);



			
		}

	}

return 0;
}
