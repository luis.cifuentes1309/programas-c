#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>

#define SIZE 200

int main(int argc, char *argv[]){

pid_t pid;
int tuberia[2];
char cadena[SIZE];
bool estado=false;

pipe(tuberia);
pid=fork();

	if(pid==-1){
	
		printf("ERROR\n");
	}else{
	
		if(pid==0){
			
			wait(NULL);
			close(tuberia[1]);
			memset(cadena,0,SIZE);
			while(read(tuberia[0],cadena,SIZE)>0){
			
				printf("El hijo lee: %s\n",cadena);
			}
			close(tuberia[1]);
		}else{
		
			while(strcmp(cadena, "FIN")){
			
			close(tuberia[0]);
			memset(cadena,0,SIZE);
			printf("Introduce un valor: \n");
			scanf("%s",cadena);
			write(tuberia[1],cadena,SIZE);
			

			}
		close(tuberia[0]);
		
		}
	}
	return 0;
}
