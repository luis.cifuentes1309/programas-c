#include <sys/types.h>	
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wait.h>
#include <fcntl.h>

#define SIZE 512

int main (int argc, char *argv[]){

	pid_t pid;
	
	int tuberia[2];
	char buffer[SIZE];
	pipe(tuberia);
	pid = fork();
	
	if(pid == -1){
		printf("ERROR\n");
	}else{
		if(pid == 0){

			//Siempre se ejecuta primero el primero que escribe por que no se puede leer algo que no se ha escrito todavia.
			//En este caso es el padre el que escribe

			close(tuberia[0]);//se cierra la tuberia que no se va a usar en este caso la de lectura

			strcpy(buffer, "Pruebo la tuberia padre-hijo\n");
			write(tuberia[1], buffer, sizeof(buffer));

			close(tuberia[1]);

			exit(0);
		}

		else{
			close(tuberia[1]);//cerramos la tuberia de escritura
			wait(NULL); //Espera a que el hijo escriba
			
			//Una tuberia tiene 2 descriptores, el primer descriptor que es el de tuberia[0] es el de lectura y el segundo descriptor 			  que es el tuberia[1]  es el de escritura

			printf("El padre lee el PIPE \n");
                read(tuberia[0], buffer, sizeof(buffer));
                printf("\t Mensaje leido: %s \n", buffer);
			
			close(tuberia[0]);
		}
	}

	return 0;
}
