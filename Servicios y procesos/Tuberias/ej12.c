#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <wait.h>
#include <sys/stat.h>

/*
 Los parametros de la funcion son:
 -numero: hijos que quedan por crear 
 -posicion: indica la posicion del array a rellenar
 -pid[]: array que guarda los PID's de los hijos

 */

void crear_hijo(int hijos_por_crear, int posicion, pid_t pid[]){
	
	int pid2;
	
	pid[posicion] = getpid();

	if(hijos_por_crear == 0){
		printf("Es el último hijo\n");
		printf("El hijo creado es %i = %i, mi padre es %i\n", getpid(), pid[posicion], getppid());

	}
	
	else{
		pid2 = fork();
		
		if (pid2 == 0){
			
			printf("El hijo creado es %i = %i, mi padre es %i\n", getpid(), pid[posicion], getppid());
			crear_hijo(hijos_por_crear-1, posicion+1, pid);
		}

		else{
			
			wait(NULL);
		}
	}
}

int main (int argc, char *argv[]){
	
	pid_t pid[6];
	int numero_hijos;

	if(argc == 2){
		numero_hijos = atoi(argv[1]);
		crear_hijo(numero_hijos, 0, pid);
	}
	
	else{
		printf("Parametros son erroneos\n");
	}

	return 0;
}
