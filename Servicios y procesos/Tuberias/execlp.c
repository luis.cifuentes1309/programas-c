#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <wait.h>

int main (int argc, char *argv[]){

pid_t pid;
int tuberia[2];

pipe(tuberia);

if (argc == 4){
pid=fork();

	if(pid==-1){
		printf("Error en la creación del hijo\n");
	}
	
	else{
		if(pid==0){
			//Hijo
			close(0);

			//Estas 3 lineas sirven para no tener que pner la | de la tuberia

			dup(tuberia[0]);
			close(tuberia[0]);
			close(tuberia[1]);
			
			execlp(argv[2],argv[2],argv[3],NULL);
		
			exit(0);
		}
		
		else{
			//Padre
			close(1);//Esto se puede obviar pero es una buena practica

			//Estas 3 lineas sirven para no tener que pner la | de la tuberia
			dup(tuberia[1]);
			close(tuberia[0]);
			close(tuberia[1]);

			execlp(argv[1],argv[1],NULL);//si comentamos esta linea no se nos muestra la salida de ls, solo la de wc

			wait(NULL);
		}
	}
}

else{
	printf("Hay error en los parametros\n");
}
	return 0;
}


