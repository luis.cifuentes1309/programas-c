#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>

void mandarSenal(int signal){
	
	printf("Mi pid es %i y el de mi padre %i\n", getpid(), getppid());
	
}

int main (int argc, char *argv[]){

	pid_t pid;

	pid = fork();

	if (pid == 0){	

		while(1){
			printf("Esperando...\n");
			sleep(1);
		}

		exit(0);
	}

	else{
		struct sigaction new;
        	new.sa_handler = &mandarSenal;
		sigaction(SIGINT, &new, NULL);
        	sigaction(SIGUSR1, &new, NULL);

		sleep(5);

		kill(pid,SIGUSR1);//Envia una señal al hijo

		pause();//Hasta que no se active la señal SIGINT (control C) se quedara parado el proceso
		wait(NULL);
	}

	return 0;
}

