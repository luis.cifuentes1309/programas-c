#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>

void manejadora (int signal){
	printf("He tratado la señal\n");
}

int main (int argc, char * argv[]){

	struct sigaction new;
	struct sigaction old;

	new.sa_handler = &manejadora;
	sigemptyset(&new.sa_mask);

	sigaction(SIGINT, &new, &old);

	while(1){
		
		printf("Estoy esperando una señal...\n");
		sleep(2);
	}

	return 0;
}
