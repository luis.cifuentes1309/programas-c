#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <unistd.h>

void handlerC(int signal){
	printf("El numero de la señal es %i\n", signal);
	
	for(int i = 0; i <= 50; i++){
		if(i%2 == 0){
			printf("%i ", i);
		}
	}

	exit(0);
}

void handlerZ(int signal2){
	printf("El numero de la es %i\n", signal2);

	printf("ADIOS!\n");

	exit(0);
}

int main(int argc, char *argv []){

	signal(SIGINT, &handlerC);

	signal(SIGTSTP, &handlerZ);

	//raise(SIGINT); ejecuta la señal sin tener que poner control C 

	while (1){
                printf("Esperando señal...\n");
                sleep(1);
        }
	
return 0;
}
