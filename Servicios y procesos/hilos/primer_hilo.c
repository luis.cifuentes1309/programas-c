#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <pthread.h>
#include <string.h>

void *funcion_hilo (void *arg){

        char *cadena;
        cadena = (char *)arg;

        printf("Hola, soy un hilo %s\n", cadena);
}

int main (int argc, char *argv[]){

        pthread_t hilo;
        int error;
        char *argumento = "Hola Mundo";

        //Primer parametro: puntero al identificador del hilo
        //Segundo parametro: Los atributos del hilo si no estan inicializados los ponemos a NULL
        //Tercer parametro: Puntero a la funcion
        //Cuarto parametro: Argumentos de la funcion(a las variables no hay que poner & pero a las estructuras si que hay que ponerle &)
        error = pthread_create(&hilo, NULL, funcion_hilo, (void *)argumento);

        if(error != 0){
                printf("Ha habido un error al crear un hilo\n");
        }

        pthread_exit(&hilo);


return 0;
}
