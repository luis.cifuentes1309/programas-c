#include <stdio.h> 
#include <stdlib.h> 
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

void * imprimir_mensaje( void *puntero ); 

int main(){

	pthread_t hilo1, hilo2; 

	char *mensaje1 = "Hola"; 
	char *mensaje2 = "Mundo"; 

	pthread_create(&hilo1,NULL,imprimir_mensaje,(void*) mensaje1); 

	pthread_create(&hilo2,NULL,imprimir_mensaje,(void*) mensaje2);

	printf("\n\n");

	printf("Press 'Enter' to continue: ... ");
	while ( getchar() != '\n');
	

	
	
	pthread_join(hilo1, NULL);//sincroniza hilos(tiene 2 parametros primero el nombre del hilo y el segundo NULL) aqui no funciona hay que ver como usarlo bien 
	//pthread_join(hilo2, NULL);

	pthread_exit(&hilo1);
	pthread_exit(&hilo2);
	exit(0); 

	return 0; 
} 

void * imprimir_mensaje( void *puntero ){
	
	char *mensaje;

	mensaje = (char *) puntero; 
	
	printf("%s ", mensaje); 
	
	return puntero; 
} 
