#include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <pthread.h>

void * hola ( void * arg ) {
	
	char * mensaje = "Hola";

	int i;

	for ( i = 0; i < strlen(mensaje); i++ ) {
		printf ( "%c" , mensaje[i]);
		fflush (stdout);
		usleep (1000000);
	}

	return NULL;
}

void * mundo ( void * arg ) {
	sleep(5);
	char * msg = "mundo";
	int i;

	printf(" ");
	for ( i = 0; i < strlen ( msg ); i ++ ) {

		printf ( "%c" , msg [i]);
		fflush (stdout) ;
		usleep (1000000);
		
	}

	printf("\n");

	return NULL ;
}

int main ( int argc , char * argv []) {	
	pthread_t h1;
	pthread_t h2;
	pthread_create(&h1 , NULL , hola ,  NULL);
	pthread_create(&h2 , NULL , mundo , NULL );

	pthread_exit(&h1);
	pthread_exit(&h2);

	printf ( "Fin \n");

	exit(0);
	return 0;
}