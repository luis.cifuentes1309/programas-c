#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>

int main (int argc, char *argv[]){

	int pid, sig;

	if (argc == 3){
		//programa señal pid
		sig = atoi(argv[1]);//atoi es para convertir a int el char *argv[]
		pid = atoi(argv[2]);

		if (kill(0,pid) == 1){
			//kill comprueba si existe un proceso 
			//si kill es  == 1 es que el pid no existe
			printf("El proceso no existe \n");
		}else{
			printf("Enviando una señal a otro proceso...\n");
			kill(sig,pid);
		}
	}else{
		printf("Los parametros están mal\n");
	}


return 0;
}
