#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <signal.h>



void hijo_creado(){
// Señal que indique si están disponibles para iniciar la comunicación.
        printf("El hijo ha sido creado \n");
}

void padre_creado(){
// Señal que indique si están disponibles para iniciar la comunicación.
        printf("El padre ha sido creado \n");
}


/*
- posicion: indica la posición del array a rellenar
- pid []: array que guarda los PID's de los hijos
*/

void crear_hijo(int hijos_por_crear, int posicion, pid_t pid[]){

	
	pid_t pid2;
	
	pid[posicion] = getpid();
	
	if(hijos_por_crear == 0){
		printf("Es el último hijo\n");
		printf("El hijo creado es %i = %i, mi padre es %i\n",getpid(),pid[posicion],getppid());
	}
	
	else{
		pid2 = fork();
		
		if (pid2 == -1){
			printf("Error al crear el proceso\n");
		}

		else if (pid2 == 0){
			printf("El hijo creado es %i, mi padre es %i\n",getpid(),getppid());
			crear_hijo(hijos_por_crear-1, posicion+1, pid);
				
			signal(SIGUSR1,hijo_creado);
			
			raise(SIGUSR1);
			//kill(getpid(), SIGUSR1);
			exit(0);
	}

		else{
			sleep(1);
			
			signal(SIGUSR1,padre_creado);

                        raise(SIGUSR1);

			wait(NULL);
		}
	}

}


int main (int argc, char* argv[]){
	
	int numero_hijos;
	pid_t pid[3];

	if(argc==2){
		numero_hijos = atoi(argv[1]);//convierte los numeros pasados por parametros en string
		crear_hijo(numero_hijos, 0, pid);
	
			
	}
	
	else{
		printf("Parametros erroneos\n");
	}

	return 0;
}
