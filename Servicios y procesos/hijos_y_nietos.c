//Padre con 3 hijos y en el primer hijo 2 nietos.
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

int main (int argc, char *argv[]){

        pid_t pid;
        int i;

        for(i = 1; i <= 3 ; i++){
                pid = fork();


                if (pid == -1){
                        printf("Ocurrio un error en el proceso\n");
                        return -1;
                }

                if (pid == 0){
                       printf("Mi pid es %i y la del padre %i\n", getpid(), getppid());
			

		       if( i == 1 ){	
		       		for(int j = 1; j <= 2; j++){
		       			pid = fork();
					
					if (pid == 0){
						printf("pid del nieto %i, pid del padre %i\n", getpid(), getppid());
						exit(0);
					}

					else{
						wait(NULL);
					}
				}
			}
		exit(0);
                }

		else{
			wait(NULL);
		}
	}
        
return 0;
}
              
