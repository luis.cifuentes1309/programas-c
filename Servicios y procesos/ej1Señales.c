#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <unistd.h>

void hander(int signal){
	printf("He recibido la señal %i \n", signal);
	exit(0);//si no ponemos esto el programa no se acaba aunque pongamos control c 
}

int main (int argc, char *argv[]){

	signal(SIGINT, &hander);
	
	while(1){
		
		printf("Espero una señal\n");
		sleep(2);
	}

	return 0;
}

