#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main( int argc,char *argv[]){
	int longitud = 0, posicion = 0;

	//primer if para verificar que hay entre 1 y 5 paremetros si no saldra parametros erroneos
	if((argc<=6)&&(argc>1)){ 
		for(int i=1;i<argc;i++)
		{
			if(longitud<strlen(argv[i]))
			{
				longitud = strlen(argv[i]);
				posicion = i;
			}
		}
	}else{
		printf("Parametros erroneos\n");
	}

	printf("El parametro más largo es %s\n",argv[posicion]);

	return 0;
}

