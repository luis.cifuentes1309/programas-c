#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

void funcion_manejadoraSIGINT(int signal)
{
	printf("La señal que se ha encontrado es %i\n", signal);
	raise(SIGKILL);
}

void funcion_kill(int signal){
	printf("Estoy matando al proceso %i", signal);
	exit(0);
}

void funcion_parar(int signal){
	printf("Estoy parando el proceso con la señal %i\n", signal);
	exit(0);

	for(int i = 0; i < 10; i++){
		printf("El valor del numero i es %i", i);
	}
}

int main(int argc, char *argv[]){

	signal (SIGINT,&funcion_manejadoraSIGINT);
	signal (SIGKILL,&funcion_kill);
	signal (SIGSTOP,&funcion_parar);

	while(1)
	{
		printf("Espero señal....\n");
		sleep(2);
	}

	return 0;
}

