#include <stdio.h>
#include "iostream"
#include "math.h"

/**************************************/
/*HALLAR HIPOTENUSA SABIENDO 2 CATETOS*/
/**************************************/

int main(){

/*DECLARACION VARIABLES*/
float cat1,	//cateto1
      cat2,	//cateto2
      hip;	//hipotenusa

/*ENTRADA DE DATOS*/
printf("Ingrese el valor de los 2 catetos \n");
scanf("%f %f", &cat1, &cat2);

/*OPERACIÓN*/
hip = sqrt( pow(cat1,2) + pow(cat2,2) );

/*SALIDA DE DATOS*/
printf("El valor de la hipotenusa es: %f \n",hip);

return 0;
}

