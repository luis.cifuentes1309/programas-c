#include <stdio.h>
#include "iostream"

int
main (){

/* DECLARACION DE VARIABLES */

int op1,			//Operando 1
    op2, 			//Operando 2
    result;			// Resultado


system ("clear"); 
system ("toilet -fpagga --gay CALCULADORA");
printf ("\n\n");

/*ENTRADA DE DATOS*/
printf ("\n Primer valor: \n");
scanf ("%d", &op1);
printf ("Segundo valor: \n");
scanf("%d", &op2);

/*CALCULOS*/
result = op1 + op2;

/*SALIDA DE DATOS*/
printf ("\n %i + %i = %i \n", op1, op2, result);

system ("figlet -f small 'algunos datos'");

printf ("\n\n");

printf("&%p: %i (%lu bytes)\n", &op1, op1, sizeof (op1) );
printf("&%p: %i (%lu bytes)\n", &op2, op2, sizeof (op2) );
printf ("Las direcciones de memoria ocupan: %lu bytes.\n", sizeof (&op1));

printf ("El tipo de datos para guardar la dirección de op1 es int *\n"); 

printf ("&op1: %p\n", &op1);
printf ("&op2: %p\n", &op2);

int *d_op1 = &op1;

*d_op1 = 9;

return 0;}
