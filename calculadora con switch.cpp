#include "iostream"
#include <stdio.h>
#include <stdlib.h>

/*DECLARACIÓN VARIABLES*/
char operad;
float num1, num2;
int main(){

/*ENTRADA DE DATOS*/
printf("Pulse 1 (para suma), 2 (resta), 3 (multiplicar), 4 (dividir): ");
scanf("%c", &operad);
printf ("\nIntroduzca el primer operando: ");
scanf("%f", &num1);
printf ("Introduzca el segundo operando: ");
scanf("%f", &num2);

/*SALIDA DE DATOS*/
switch(operad){
case '1': printf("\nEl resultado es: %.2f + %.2f = %.2f\n",num1, num2,num1 + num2);break;
case '2': printf("\nEl resultado es: %.2f - %.2f = %.2f \n", num1, num2, num1 - num2);break;
case '3': printf("\nEl resultado es: %.2f * %.2f = %.2f \n", num1, num2, num1 * num2);break;
case '4': printf("\nEl resultado es: %.5f / %.5f = %.5f \n", num1, num2, num1 / num2);break;
default:printf("Operando incorrecto \n");
}

return 0;
}
