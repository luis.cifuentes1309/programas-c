#include<stdio.h>

int  main(){

/*DECLARACION VARIABLES*/
double result,
       num1,
       num2,
       num3;

/*ENTRADA DE DATOS*/
printf("Introduzca 3 numeros para hallar su media aritmética \n");
scanf("%lf %lf %lf",&num1,&num2,&num3);

/*OPERACIONES*/
result = (num1 + num2 + num3)/3;

/*SALIDA DE DATOS*/
printf("La media entre  %lf, %lf y %lf es %lf \n", num1, num2, num3, result); 

return 0;
}

