#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int suma (int op1, int op2) {
    return op1 + op2;
}

int resta (int op1, int op2) {
    return op1 - op2;
}

int preguntar_op () {
    static int nop = 0; //nop --> NUMERO DE OPERANDOS
    int op;
    

    printf("Operando %i: ", ++nop);
    scanf(" %i", &op);

    return op;
}

int tipo_operac (){
    char operacion;
printf("¿Quieres sumar o restar?(s/r)\n");
__fpurge(stdin);
scanf("%c",&operacion);

return operacion;
}

int main() {

     /* DECLARACIÓN DE VARIABLES */
    int op1,      // Operando 1
        op2,      // Operando 2
        result;
    char operacion;



    /* ENTRADA DE DATOS */
    op1 = preguntar_op ();
    op2 = preguntar_op ();

  /* SUMA O RESTA*/
operacion = tipo_operac();
 
 /*CALCULOS Y SALIDA DE DATOS*/   
if (operacion == 's'){
result = suma (op1, op2);
    printf("%i + %i = %i\n", op1, op2, result);
}
else if (operacion == 'r'){
result = resta (op1, op2);
      printf("%i - %i = %i\n", op1, op2, result);
}
else{ 
printf("Paremetro incorrecto\n");
}


    return EXIT_SUCCESS;

}
