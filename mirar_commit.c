#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define   MAX   0x20


int main (int argc, char *argv[]) {

int num;
    
  printf("%.*s\n",4,"hello world"); //Imprime las 4 primeras letras de hello world (hell)
  
  printf("%*d\n", 8, 10);//si un num ocupa menos de 8 digitos(en este caso), se imprimen caracteres de espacio hasta que se llena el ancho de 8.
  
  printf("Introduce 2 numeros \n");
  scanf("%*d %d", &num); //El primer numero se lee pero se ignora y solo queda el segundo num pasa lo mismo con %*s %s
  printf("El segundo numero es %i",num);
    return EXIT_SUCCESS;
}
