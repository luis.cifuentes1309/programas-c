#include <stdio.h>
#include <stdlib.h>

int suma (int op1, int op2);
int rest (int op1, int op2);
int mult (int op1, int op2); 
int divi (int op1, int op2);

int (*catalogo[])(int, int) = { &suma, &rest, &mult, &divi };

//int (*f[3])(int,int); 3 celdas conteniendo punteros a funciones

enum TOp { sum, rst, mlt, dvd, TOT_OP };

const char *menu_opt[] = {
	"1.- suma",
	"2.- resta",
	"3.- multiplicación",
	"4.- division",
	NULL
};

void title () {
	system ("clear");
	system ("toilet -fpagga --gay CALCULATOR");
	printf("\n\n\n");
}

enum TOp menu (){
	unsigned elegido;
	const char **p_menu = menu_opt;

	while (*p_menu != NULL){
		printf("\t\t%s\n", *p_menu);
		p_menu++;
	}
	
	printf ("\n\n");
	printf ("\tOpcion: ");
	scanf ("%u", &elegido);

	return (enum TOp) (elegido -1);
}



int suma (int op1, int op2) { return op1 + op2; }
int rest (int op1, int op2) { return op1 - op2; }
int mult (int op1, int op2) { return op1 * op2; }
int divi (int op1, int op2) { return op1 / op2; }

int preg_op (const char *pregunta) {
	int result;
	
	printf("%s: ", pregunta);
	scanf("%i",&result);
	
	return result;
}

int main(int argc, char *argv[]) {
	int a, b;
	enum TOp chosen;
	int resultado;
	
	title ();
	
	chosen = menu ();
	a = preg_op ("Operando 1");
	b = preg_op ("Operando 2");
	
	resultado = (*catalogo[chosen])(a,b);
	
	printf("Resultado= %i \n",resultado);
	
return 0;
}
