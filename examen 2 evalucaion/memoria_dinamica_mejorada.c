#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
   
    enum TPieza {libre, peon, torre, caballo, alfil, reina, rey};
       
    typedef enum TPieza TipoPieza;
       
    //rellenar de ceros la matriz memoria estatica
    
   /* TipoPieza tablero[8][8];
       
    for (int fila=0;fila<8;fila++)
        for (int columna=0;columna<8;columna++){
           tablero[fila][columna] = libre;
            printf("tablero[%i][%i] = %i\n", fila, columna, tablero[fila][columna]);
        }
   */

   
  //rellenar de ceros la matriz memoria dinámica
  
  int  reservar_memoria;
  printf("¿Cuanta memoria quieres reservar?\n");
  scanf("%i",&reservar_memoria);
  
  printf("\nTipoPieza tiene %lu bytes\n",sizeof(TipoPieza));


  TipoPieza *tablero_negras = (TipoPieza*) malloc(reservar_memoria*sizeof(TipoPieza));
  
  if (tablero_negras == NULL){
      printf("Fallo al reservar memoria\n");
      return 1; //El return sirve para salir del programa
  }
   
   bzero(tablero_negras,sizeof(tablero_negras));
   printf("\nSe ha reservado memoria correctamente\n");
   
   free(tablero_negras);
   
    return 0;
}

