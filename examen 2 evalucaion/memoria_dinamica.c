#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
   
    enum TPieza {libre, peon, torre, caballo, alfil, reina, rey};
       
    typedef enum TPieza TipoPieza;
       
    
    //rellenar de ceros la matriz (memoria estática)
    
   /* TipoPieza tablero[8][8];
       
    for (int fila=0;fila<8;fila++)
        for (int columna=0;columna<8;columna++){
           tablero[fila][columna] = libre;
            printf("tablero[%i][%i] = %i\n", fila, columna, tablero[fila][columna]);
        }
   */
   
  
  //rellenar de ceros la matriz (memoria dinámica)
  
   TipoPieza *tablero_negras;
   
   tablero_negras = (TipoPieza*) malloc(64*sizeof(TipoPieza));
   bzero(tablero_negras,sizeof(tablero_negras));
   free(tablero_negras);
   
    return 0;
}

